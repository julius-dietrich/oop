package Aufgabenblatt2;

public class Farbe {

    //private Attribute
        private int rot;
        private int gruen;
        private int blau;

    public Farbe()
    {
        this.rot=0;
        this.blau=0;
        this.gruen=0;
    }

    //Getter und Setter
    public int getRot() {
        return rot;
    }

    public void setRot(int rot) {
        this.rot = rot;
    }

    public int getGruen() {
        return gruen;
    }

    public void setGruen(int gruen) {
        this.gruen = gruen;
    }

    public int getBlau() {
        return blau;
    }

    public void setBlau(int blau) {
        this.blau = blau;
    }

    public static void main ( String [] args ) {
        Farbe dieFarbe = new Farbe();
        Farbe gruen = new Farbe();
        System.out.println(gruen);

    }
}


