package test;
/****************************************************
Name ## Nachname ## Matrikelnummer ## Gruppennummer
Luca    Znaniewicz  933127            I1-Delta
Julius  Dietrich    932906            I1-Delta
*****************************************************/
public class Aufgabe1 {
    public static void main ( String [] args ) {

        double zufall =(Math.random()*200)-100;

        if(zufall>0)
            System.out.println("Die Zahl ist positiv " +zufall);

        else
            System.err.println("Die Zahl ist negativ " +zufall);
    }
}
