package test;
/****************************************************
 Name ## Nachname ## Matrikelnummer ## Gruppennummer
 Luca    Znaniewicz  933127            I1-Delta
 Julius  Dietrich    932906            I1-Delta
 *****************************************************/
public class Aufgabe5 {
    public static void main ( String [] args ) {
        if(args.length !=0)
        {
            int max = Integer.parseInt(args[0]);
            for(int i=1; i < args.length; i++)
            {
                if(Integer.parseInt(args[i]) > max)
                {
                    max = Integer.parseInt(args[i]);
                }
            }System.out.println("die groeßte Zahl ist: " + max);

        }else{
            System.out.println("Das Programm braucht mehr Infos");
        }

    }
}
